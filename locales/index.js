module.exports = {
  ca: 'Català',
  cs: 'Czech',
  de: 'Deutsch',
  en: 'English',
  es: 'Español',
  eu: 'Euskara',
  fr: 'Francais',
  gl: 'Galego',
  it: 'Italiano',
  nb: 'Norwegian Bokmål',
  nl: 'Dutch',
  pl: 'Polski',
  pt: 'Português',
  'pt-BR': 'Português (Brasilian)',
  ru: 'Русский',
  sk: 'Slovak',
  zh: '中国'
}
